import 'package:flutter/material.dart';

const conBottomContainerHeight = 80.0;
const conActiveCardColor = Color(0xFF1d1E33);
const conInactiveCardColor = Color(0xFF111328);
const conBottomContainerColor = Color(0xFFEB1555);
const conTextColor = Color(0xFF8D8E98);
const conRoundButtonBackgroundColor = Color(0xFF4C4F5E);

const conTextStyle = TextStyle(
  fontSize: 18.0,
  color: Color(0xFF8D8E98),
);

const conHeightContainerTextStyle = TextStyle(
  fontSize: 80.0,
  fontWeight: FontWeight.w900,
);

const conCalculateButtonTextStyle = TextStyle(
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
);

const conTitleTextStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.bold,
);

const conResultHeadTextStyle = TextStyle(
  color: Color(0xFF24D876),
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);

const conBMITextStyle = TextStyle(
  fontSize: 100.0,
  fontWeight: FontWeight.bold,
);

const conDescTextStyle = TextStyle(
  fontSize: 22.0,
);
