import 'package:flutter/material.dart';

class CardContainer extends StatelessWidget {
  CardContainer({@required this.cardColor, this.cardWidget, this.onPress});

  final Color cardColor;
  final Widget cardWidget;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        child: cardWidget,
        margin: EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          color: cardColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }
}
